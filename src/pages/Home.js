import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import '../components/highlights.css';

export default function Home() {
  const data = {
    title: "Market Education",
    content: "Curated for individuals who want to improve portfolio performance",
    destination: "/products",
    label: "Enroll now!"
  }

  return (
    <>
      <div className="home-container">
        <div className="col-6 pt-3 pb-3" id="home-left">
          <Banner data={data} />
        </div>
        <div className="col-6" id="home-right">
          <Highlights />
        </div>
      </div>
      <div className="footer">
     
      </div>
    </>
  )
}
