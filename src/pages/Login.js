import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import '../components/navbar.css';

export default function Login() {

    // Allows us to consume the UserContext object and it's properties for user validation
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of our input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether the submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);

    // useEffect() - whenever there is a change in our webpage
    useEffect (() => {

      // validation to enable the register button when all fields are populated and both password match.
      if(email !== '' && password !== '') {

        setIsActive(true)

      } else {

        setIsActive(false)

      }
    })

    function loginUser(e) {

      e.preventDefault();

 
      fetch('http://localhost:3001/users/login', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res => res.json())
      .then(data => {

        // it is a good practice to always print out the result of our fetch request to ensure that the correct information is received in our front end application.
        console.log(data)

        if (typeof data.access !== "undefined") {

          // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                  localStorage.setItem('token', data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Authentication Successful",
            icon: "success",
            text: "Welcome to FirstMetroSec Market Education!"
          })

        } else {

          Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login details and try again!"
          })

        }

      })


      // clear input fields
      setEmail('');
      setPassword('');

      // alert('Successful login');
    }

    const retrieveUserDetails = (token) => {

     fetch('http://localhost:3001/users/details', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })

    }

    return (
      (user.id !== null) ?
        <Navigate to="/products" />
      :
      <Form onSubmit={e => loginUser(e)} className="d-flex justify-content-center bg-light" id="login">

      <Card className="card shadow text-center col-sm-12 col-md-8 col-lg-8 col-xl-4 mb-5 p-3">
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control className="form-control form-control-sm" type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
            
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
          </Form.Group>

          {
            isActive ?
              <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
            :
              <Button variant="dark" type="submit" id="submitBtn" disabled>Submit</Button>
          }

           <p className="text-center mt-3">
          No account yet? Click <Link to="/register"> here</Link> to register.
         </p>
          </Card>
         

        </Form>
    )
}