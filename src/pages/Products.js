/*import { useState, useEffect } from 'react';
// import CourseData from '../data/CourseData';
import ProductCard from '../components/ProductCard';
import '../components/navbar.css';
import { Container, Row, Col } from 'react-bootstrap';


export default function Products() {

	// console.log(CourseData);

	// State that will be used to store the courses retrieved from the database
    const [ products, setProducts ] = useState([]);

	// Retrieves the courses from the database upon initial render of the "Courses" component
	useEffect(() => {
    fetch('http://localhost:3001/products/active')
    .then(res => res.json())
    .then(data => {
        setProducts(data.map(product => (
            <ProductCard key={product.id} productProp={product} />
        )))
    })
}, [])



	return (
<>
			
			<h1 id="product-text">Products</h1>
			{products}
		</>
	)
}


*/


import { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import '../components/navbar.css';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3001/products/active')
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <Container>
      <h1 id="product-text">Products</h1>
      <Row>
        {products.map((product) => (
<Col xs={12} sm={6} md={4} key={product.id}>
<ProductCard productProp={product} />
</Col>
))}
</Row>
</Container>
);
}


