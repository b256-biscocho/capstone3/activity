/*import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

    export default function UpdateProduct() {

    
      
      const { user } = useContext(UserContext); 
      const { productId } = useParams(); 
      const [name, setName] = useState('');
      const [image, setImage] = useState('');
      const [description, setDescription] = useState('');
      const [price, setPrice] = useState(0);
      const [isAvailable,setIsAvailable] = useState(true);
      const [createdOn, setCreatedOn] = useState('');
      const [isActive, setIsActive] = useState(false);
      const [updatedName, setUpdatedName] = useState('');
      const [updatedDescription, setUpdatedDescription] = useState('');
      const [updatedImage, setUpdatedImage] = useState('');
      const [updatedPrice, setUpdatedPrice] = useState(0);

      useEffect(() => {

       
            if(updatedName !== '' || updatedDescription !== ''|| updatedPrice !== 0 ){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [updatedName, updatedDescription, updatedPrice, updatedImage]);

       
        fetch(`http://localhost:3001/products/${productId}`)
        .then(res => res.json())
        .then(data => {

          console.log(data);
          setName(data.name);
          setDescription(data.description);
          setPrice(data.price);
          setIsAvailable(data.isActive);
          setImage(data.image);
          setCreatedOn(data.createdOn);
        }) 

// Update Product

function productUpdate(e) {

  e.preventDefault()


 fetch(`http://localhost:3001/products/update/${productId}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            name: updatedName,
            description: updatedDescription,
            price: updatedPrice,
            image: updatedImage
        })

    })
    .then(res => res.json())
    .then(data => {

            if(data) {

                Swal.fire({
                    title: "You have successfully updated this product!",
                    icon: "success",
                    text: "Please check product list"
                
                })

                setUpdatedName('')
                setUpdatedDescription('')  
                setUpdatedPrice('')
                setUpdatedImage('')

            } else {

                Swal.fire({
                    title: "Unexpected Error found!",
                    icon: "success",
                    text: "Please try again!"
                
                })
            }

    })

}


      return(
        
        <Container className="mt-5 text-align-center">
          <Row className>
            <Col md={6}>
              <Card>
                <Card.Body className="text-align-center">
                  <Card.Title>{name}</Card.Title> <br/>
                   <Card.Img variant="top" src={image} style={{ width: '10rem', height: '12rem'  }}/> <br/><br/>
                  <Card.Subtitle>Description:</Card.Subtitle>
                  <Card.Text>{description}</Card.Text>
                  <Card.Subtitle>Price:</Card.Subtitle>
                  <Card.Text>PhP {price}</Card.Text>
                  <Card.Subtitle>Available:</Card.Subtitle>
                  <Card.Text>{isAvailable ? "Yes" : "No"}</Card.Text>
                  <Card.Subtitle>Created On:</Card.Subtitle>
                  <Card.Text>{createdOn}</Card.Text>
                </Card.Body>    
              </Card>
            </Col>
            <Col md={6}>
              <>
                <h1> Update Form </h1>
                <Container fluid>
                <Form onSubmit={e => productUpdate(e)}>
                <Form.Group className="mb-3" controlId="nameProduct">
                          <Form.Label>Product Name</Form.Label>
                          <Form.Control type="text" placeholder="Enter new product name" value={updatedName} onChange={e => setUpdatedName(e.target.value)}/>
                          <Form.Text className="text-muted">
                          </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="descriptionProduct">
                          <Form.Label>Description</Form.Label>
                          <Form.Control style={{ height: '12rem' }} as="textarea" placeholder="Enter new product description" value={updatedDescription} onChange={e => setUpdatedDescription(e.target.value)}/>
                          <Form.Text className="text-muted">
                          </Form.Text>
                        </Form.Group>

                    <Form.Group className="mb-3" controlId="ImageProduct">
                          <Form.Label>Image URL</Form.Label>
                          <Form.Control type="text" placeholder="Enter new image url" value={updatedImage} onChange={e => setUpdatedImage(e.target.value)}/>
                        </Form.Group>
                    <Form.Group className="mb-3" controlId="priceProduct">
                          <Form.Label>Price</Form.Label>
                          <Form.Control type="text" placeholder="Enter new product price" value={updatedPrice} onChange={e => setUpdatedPrice(e.target.value)}/>
                        </Form.Group>
                         {
                          isActive ?
                          <>
                              <Button variant="primary" type="submit" id="submitBtn">Submit</Button>{' '}
                              <Button variant="primary" type="submit" id="submitBtn" as={Link} to={'/adminDashboard'}>Back to Admin Dashboard</Button>
                            </>
                          :
                          <>
                              <Button variant="danger" type="submit" id="submitBtn" disabled>Update</Button>{' '}
                              <Button variant="primary" type="submit" id="submitBtn" as={Link} to={'/adminDashboard'}>Back to Admin Dashboard</Button>
                           </>
                        }
                  </Form>
                  </Container>
              </>
            </Col>
          </Row>
        </Container>
  
      )
    }
*/





import { useState, useEffect, useContext } from 'react';
import AllProducts from '../components/ProductCard';
import UserContext from '../userContext';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { Button, Row, Col, Modal, Form, Card, Table, ButtonGroup, Container, Image } from 'react-bootstrap';
import { RiEyeLine, RiArrowGoBackLine, RiCheckLine, RiCloseLine } from 'react-icons/ri';
import Swal from 'sweetalert2';

export default function AdminDashboard() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [productId, setProductId] = useState("");
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [image, setImage] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [allProducts, setAllProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (name !== '' && description !== '' && price !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price, image]);

  // Show AllProducts
  const fetchAllProducts = () => {
    fetch('http://localhost:3001/products/all')
      .then(res => res.json())
      .then(data => {
        setAllProducts(data);
      });
  };

  useEffect(() => {
    fetchAllProducts();
  }, []);

  // CreateProduct Modal
  const [addShow, setAddShow] = useState(false);

  const addHandleClose = () => setAddShow(false);
  const addHandleShow = () => setAddShow(true);

  // End of CREATE PRODUCT Modal

  function createProduct(e) {
    e.preventDefault();

    const productData = {
      name: name,
      description: description,
      price: parseFloat(price),
      image: image,
    };

    fetch('http://localhost:3001/products/create', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(productData),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'You have successfully added a new product!',
            icon: 'success',
            text: 'Product list has been updated',
          });

          setName('');
          setDescription('');
          setPrice('');
          setImage('');
        } else {
          Swal.fire({
            title: 'Unexpected Error found!',
            icon: 'success',
            text: 'Please try again!',
          });
        }
      });
  }

  const archiveProduct = (productId) => {
    fetch(`http://localhost:3001/products/archive/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Product is successfully archived!',
            icon: 'success',
            text: 'The product is now in the archive folder',
          });

          fetchAllProducts();
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again later!',
          });
        }
      });
  };

  const reactivateProduct = (productId) => {
    fetch(`http://localhost:3001/products/reactivate/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Product is successfully reactivated!',
            icon: 'success',
            text: 'The product is now up and running',
          });

          fetchAllProducts();
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again later!',
          });
        }
      });
  };

  const openEditModal = (productId) => {
    setProductId(productId);
    setShowModal(true);
  };

  const closeEditModal = () => {
    setShowModal(false);
  };

  const updateProduct = () => {
    const updatedName = document.getElementById('updatedName').value;
    const updatedDescription = document.getElementById('updatedDescription').value;
    const updatedPrice = document.getElementById('updatedPrice').value;
    const updatedImage = document.getElementById('updatedImage').value;

    fetch(`http://localhost:3001/products/update/${productId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: updatedName,
        description: updatedDescription,
        price: updatedPrice,
        image: updatedImage,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Product is successfully updated!',
            icon: 'success',
            text: 'The product information has been updated',
          });

          fetchAllProducts();
          closeEditModal();
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again later!',
          });
        }
      });
  };

  return (
    <>
      {user.isAdmin !== true ? (
        navigate('/UnauthorizedAccess')
      ) : (
        <>
          <div>
            <Row className="text-center m-3">
              <Col>
                <h1>Admin Dashboard</h1>
                <Button variant="primary" onClick={addHandleShow}>
                  Add New Product
                </Button>{" "}
                <Button variant="info">Show User Orders</Button>{" "}
              </Col>
            </Row>
          </div>
          <div>
            <Row className="text-center m-2">
              <Col>
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Image</th>
                      <th>Description</th>
                      <th>Price</th>
                      <th>Available</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {allProducts.map((product) => (
                      <tr key={product._id}>
                        <td>{product.name}</td>
                        <td>
                          <Image src={product.image} style={{ width: '6rem' }} />
                        </td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                        <td>{product.isActive ? "Yes" : "No"}</td>
                        <td>
                          <Button variant="info" onClick={() => openEditModal(product._id)}>
                            <RiEyeLine /> Edit
                          </Button>{" "}
                          <ButtonGroup>
                            {product.isActive ? (
                              <>
                                <Button variant="primary" onClick={() => archiveProduct(product._id)}>
                                  <RiCloseLine /> Deactivate
                                </Button>
                                <Button variant="outline-primary" disabled>
                                  <RiCheckLine /> Activate
                                </Button>
                              </>
                            ) : (
                              <>
                                <Button variant="outline-primary" disabled>
                                  <RiCloseLine /> Deactivate
                                </Button>
                                <Button variant="primary" onClick={() => reactivateProduct(product._id)}>
                                  <RiCheckLine /> Activate
                                </Button>
                              </>
                            )}
                          </ButtonGroup>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </Col>
            </Row>
          </div>

          {/* ADD PRODUCT MODAL */}
          <Modal show={addShow} onHide={addHandleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Add New Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container fluid>
                <Form onSubmit={(e) => createProduct(e)}>
                  <Form.Group className="mb-3" controlId="nameProduct">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter product name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="descriptionProduct">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      style={{ height: '12rem' }}
                      as="textarea"
                      placeholder="Enter product description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="priceProduct">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter product price"
                      value={price}
                      onChange={(e) => setPrice(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="imageProduct">
                    <Form.Label>Image Url</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter image url"
                      value={image}
                      onChange={(e) => setImage(e.target.value)}
                    />
                  </Form.Group>

                  {isActive ? (
                    <Button variant="primary" type="submit" id="submitBtn">
                      Submit
                    </Button>
                  ) : (
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Submit
                    </Button>
                  )}
                </Form>
              </Container>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={addHandleClose} as={Link} to={'/adminDashboard'}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
          {/* END of ADD PRODUCT MODAL */}

          {/* EDIT PRODUCT MODAL */}
          <Modal show={showModal} onHide={closeEditModal}>
            <Modal.Header closeButton>
              <Modal.Title>Edit Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container fluid>
                <Form>
                  <Form.Group className="mb-3" controlId="updatedName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control type="text" defaultValue={name} />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="updatedDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control style={{ height: '12rem' }} as="textarea" defaultValue={description} />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="updatedPrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="text" defaultValue={price} />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="updatedImage">
                    <Form.Label>Image Url</Form.Label>
                    <Form.Control type="text" defaultValue={image} />
                  </Form.Group>

                  <Button variant="primary" onClick={updateProduct}>
                    Update
                  </Button>
                </Form>
              </Container>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeEditModal}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
          {/* END of EDIT PRODUCT MODAL */}
        </>
      )}
    </>
  );
}