import { useState } from 'react';
import { Container, Row, Col, Button, Form, FormControl, Alert } from 'react-bootstrap';
import '../components/navbar.css';
export default function CreateProduct() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [error, setError] = useState('');
  const [success, setSuccess] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    setError('');
    setSuccess(false);

    // Validate form inputs
    if (!name || !description || !price) {
      setError('Please fill in all fields');
      return;
    }

    const product = {
      name,
      description,
      price: parseFloat(price)
    };

    // Perform the fetch request and handle the response
    fetch('http://localhost:3001/products/create', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(product)
    })
      .then((res) => res.json())
      .then((data) => {
        // Handle response data
        setSuccess(true);
        setName('');
        setDescription('');
        setPrice('');
      })
      .catch((error) => {
        setError('An error occurred while creating the product');
        console.log(error);
      });
  };

  return (
    <Container>
      <Row>
        <Col>
          <h1 id="new">Create Product</h1>
          {error && <Alert variant="danger">{error}</Alert>}
          {success && <Alert variant="success">Product created successfully</Alert>}
          <Form onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <FormControl
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <FormControl
                as="textarea"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price</Form.Label>
              <FormControl
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>
            <Button className="m-2" type="submit">Submit</Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}