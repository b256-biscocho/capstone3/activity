
// destructuring react bootstrap components
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './navbar.css';

export default function Banner({data}) {

    const {title, content, destination, label} = data;
    console.log(data);

	return(
		<Row>
			<Col className="p-5 text-center" id="home-text">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button id="home-button" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>
	)
}