import { Carousel } from "react-bootstrap";
import { Link, NavLink } from 'react-router-dom';
import GiftImage from "./GIFT.png";
import BestImage from "./BEST.jpg"
import TPImage from "./TP.png";
import GIFT from "./GIFT-PC.jpg"
import './navbar.css';
import './highlights.css';




export default function Highlights() {
  return (
    <div className="carousel-container">
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={GiftImage}
            alt="Guided Investor, Fearless Trader"
          />
         
        </Carousel.Item>

        <Carousel.Item>
          <img
            className="d-block w-100"
            src={BestImage}
            alt="Basic Education on Stock Trading"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={GIFT}
            alt="Basic Education on Stock Trading"
          />
        </Carousel.Item>

       
      </Carousel>
    </div>
  );
}