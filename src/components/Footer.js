import React from 'react';
import { Card } from 'react-bootstrap';
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow
} from 'mdb-react-ui-kit';

import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';



export default function Footer() {
  return (
    <MDBFooter bgColor='light' className='text-center text-lg-left'>
      <MDBContainer>
        <MDBRow>
          <MDBCol lg='6' md='12' className='mb-4 mb-md-0'>
          
          </MDBCol>

          <MDBCol lg='6' md='12' className='mb-4 mb-md-0 text-justify'>
            
            <h2 className='text-uppercase'>Meet the Instructor</h2>

            <p className="p-2 text-justify">
             Aaron Say is a Chartered Market Technician (CMT), Certified Public Accountant (CPA), and also a Certified Internal Auditor (CIA). He is also the market consultant of FirstMetroSec and a graduate of Applied Economics and Accountancy from De La Salle University Manila, Aaron was a former economist with the Bangko Sentral ng Pilipinas (BSP) and has over a decade of experience in trading the US and Philippine stock markets. Retired from corporate life at the age of 29, he now has his own investment advisory firm, Rhyme and Reason Investment Research and Management. Aside from his consultancy work, he is an educator for the Philippine Stock Exchange and a regular resource speaker in ANC and Bloomberg or One News.
            </p>
          
          </MDBCol>
        </MDBRow>
      </MDBContainer>

      <div className='text-center pt-3 pb-3'>
        &copy; {new Date().getFullYear()} Copyright:{' '}
        <a className='text-dark' href='https://firstmetrosec.com.ph/'>
          MDBootstrap.com
        </a>
      </div>
    </MDBFooter>
  );
}