import { useContext } from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext';
import logo from './logo.png';
import './navbar.css';

export default function AppNavBar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg" className="sticky-top shadow-lg justify-content-end">
      <Container>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="navbar-toggler" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/" className="nav-link">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" className="nav-link">
              Courses
            </Nav.Link>
          {user.id !== null && user.isAdmin && (
              <Nav.Link as={NavLink} to="/admindashboard">
                Admin Dashboard
              </Nav.Link>
            )}
          </Nav>
          <Nav>
            {user.id !== null ? (
              <NavDropdown title={user.username} id="basic-nav-dropdown" className="nav-link dropdown-menu-right">
                <NavDropdown.Item as={NavLink} to="/profile" className="dropdown-item">
                  Profile
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/logout" className="dropdown-item">
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login" className="nav-link">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" className="nav-link">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
        <Navbar.Brand as={Link} to="/" className="navbar-logo ml-auto" id="fms-logo">
          <img src={logo} alt="Logo" />
        </Navbar.Brand>
      </Container>
    </Navbar>
  );
}

/*import { useContext } from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext';
import logo from './logo.png';
import './navbar.css';

export default function AppNavBar() {
  const { user } = useContext(UserContext);

  return (


    <Navbar bg="light" expand="lg" className="sticky-top shaddow-lg">
      <Container>
        <Navbar.Brand as={Link} to="/" className="navbar-logo">
          <img src={logo} alt="Logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/" className="justify-content-end">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" className="justify-content-end">
              Courses
            </Nav.Link>
            {user.id !== null && user.isAdmin && (
              <Nav.Link as={NavLink} to="/admindashboard">
                Admin Dashboard
              </Nav.Link>
            )}
          </Nav>
          <Nav>
            {user.id !== null ? (
              <NavDropdown title={user.username} id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/profile">
                  Profile
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/logout">
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>



 );
}*/



/*


import { useContext } from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext';
import './navbar.css'
import logo from './logo.png'; // Update the path to your logo image



export default function AppNavBar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
      <Container>
          <Navbar.Brand as={Link} to="/" className="navbar-logo">
          <img src={logo} alt="Logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products">
              Courses
            </Nav.Link>
            {user.id !== null && user.isAdmin && ( // Conditional render for Admin Dashboard link
              <Nav.Link as={NavLink} to="/admindashboard">
                Admin Dashboard
              </Nav.Link>
            )}
            {user.id !== null ? (
              <Nav.Link as={NavLink} to="/logout">
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}



*/