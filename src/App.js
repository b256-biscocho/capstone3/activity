import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './userContext';
import AppNavBar from './components/AppNavBar';
import Footer from './components/Footer';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import ProductView from './pages/ProductView';
import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './pages/CreateProduct';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  });

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/admindashboard" element={<AdminDashboard />} />
            <Route path="/createproduct" element={<CreateProduct />} />
        
            <Route path="/productview/:productId" element={<ProductView />} />
            <Route path="*" element={<NotFound />} />
            <Route path="/footer" element={<Footer />} />

          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;